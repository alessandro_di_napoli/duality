﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIScript : MonoBehaviour
{
    public Text conditions, winText, loseText;
    public int sceneIndex;
    private void Start()
    {
        sceneIndex= SceneManager.GetActiveScene().buildIndex;
    }

    private void Update()
    {
        SetText();
    }

    private void SetText()
    {
        switch (sceneIndex)
        {
            case 0:
            conditions.text = "Woah, It's pretty hot in here, enjoy the sauna and be careful to not Jump with SPACEBAR.";
            break;
        }
    }
}
